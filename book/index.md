# Introdução à Computação Quântica

Este é um material didático auxiliar para a disciplina FSC7152 – Computação Quântica da UFSC, que tem como objetivo estabelecer uma ponte entre os conceitos teóricos vistos em sala de aula e a prática da programação quântica utilizando a [plataforma Ket](https://quantumket.org).

Parte deste material é adaptada das notas de aula do Prof. Dr. Eduardo Inácio Duzzioni e do TCC de Giovani Goraiebe Pollachini.

**CONTEÚDO**

```{tableofcontents}
```
